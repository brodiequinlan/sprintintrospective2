<?php
    $conn = new mysqli('localhost:3306','root','bob','users');
    $data = array();
    if (isset($_GET['sprint_id'])) 
    {
        $sql = 'SELECT * FROM sprint_backlog_junction where sprintId = ' . $_GET['sprint_id'];
        $result = $conn->query($sql);
        $id = 0;
        while($row = $result->fetch_assoc())
        {
            $temp_sql = 'SELECT * from backlog where id = ' . $row['featureId'];
            $temp_res = $conn->query($temp_sql);
            $data[$id] = $temp_res->fetch_assoc();
            $id++;
        }
        
    }
    echo json_encode($data, JSON_PRETTY_PRINT);
?>