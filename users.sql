-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2019 at 08:09 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `users`
--

-- --------------------------------------------------------

--
-- Table structure for table `backlog`
--

CREATE TABLE `backlog` (
  `id` int(11) NOT NULL,
  `description` varchar(256) NOT NULL,
  `creator` varchar(32) NOT NULL,
  `type` varchar(16) NOT NULL,
  `points` int(11) NOT NULL,
  `projectId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backlog`
--

INSERT INTO `backlog` (`id`, `description`, `creator`, `type`, `points`, `projectId`) VALUES
(909506015, 'as a programmer, i want to test my project', 'bob', 'Feature', 34234, 1885702530);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `name` varchar(16) NOT NULL,
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`name`, `id`, `owner_id`) VALUES
('test project', 1885702530, 972273110);

-- --------------------------------------------------------

--
-- Table structure for table `project_users_junction`
--

CREATE TABLE `project_users_junction` (
  `userId` int(11) NOT NULL,
  `projectId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sprints`
--

CREATE TABLE `sprints` (
  `id` int(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `projectId` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sprints`
--

INSERT INTO `sprints` (`id`, `name`, `projectId`) VALUES
(483437372, 'test sprint', 1885702530);

-- --------------------------------------------------------

--
-- Table structure for table `sprint_backlog_junction`
--

CREATE TABLE `sprint_backlog_junction` (
  `featureId` int(64) NOT NULL,
  `sprintId` int(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sprint_backlog_junction`
--

INSERT INTO `sprint_backlog_junction` (`featureId`, `sprintId`) VALUES
(909506015, 483437372);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `salt` varchar(64) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`username`, `password`, `salt`, `id`) VALUES
('bob', '123daa37e3b240787adb4d139017582829e10fae61172fa5592e8fc5bc26a462', 'b43f19a99be4d870503d0e37af0b902c', 972273110);

-- --------------------------------------------------------

--
-- Table structure for table `user_timelog`
--

CREATE TABLE `user_timelog` (
  `userId` int(11) NOT NULL,
  `featureId` int(11) NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_timelog`
--

INSERT INTO `user_timelog` (`userId`, `featureId`, `time`) VALUES
(909506015, 972273110, 523542);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
