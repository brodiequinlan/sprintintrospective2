<?php
session_start();
if(!isset($_SESSION['username']))
{
    header('Location: index.php');
}
if(isset($_POST['btnUser']))
{
    $conn = new mysqli('localhost:3306','root','bob','users');
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    echo $conn->error;
	
    $username = $_POST['username'];
    
    $sql = 'update user_info set username = "'. $username . '" where username ="' . $_SESSION['username'] . '"';
    if ($conn->query($sql) === TRUE)
    {
        $_SESSION['username'] = $username;
    }
    else
    {
        die($_SESSION['username']);
    }

	$conn->close();
	header('Location: home.php');
	die;
}
?>

<?php
//session_start();
if(isset($_POST['btnEmail']))
{
    $conn = new mysqli('localhost:3306','root','bob','users');
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    echo $conn->error;
	
    $email = $_POST['email'];
    
    $sql = 'update user_info set email = "'. $email . '" where username ="' . $_SESSION['username'] . '"';
    if ($conn->query($sql) === TRUE)
    {
    }
    else
    {
        die($_SESSION['username']);
    }
	$conn->close();
	header('Location: home.php');
	die;
}
?>

<?php
//session_start();
if(isset($_POST['btnPassword']))
{
    $conn = new mysqli('localhost:3306','root','bob','users');
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    echo $conn->error;
	
    $password = $_POST['password'];
    $salt = md5(uniqid(mt_rand(),true));
    $sh_password = hash('sha256', $password . $salt);
    
    $sql = 'update user_info set password = "'. $sh_password . '" where username ="' . $_SESSION['username'] . '"';
    if ($conn->query($sql) === TRUE)
    {
        $sql = 'update user_info set salt = "'. $salt . '" where username ="' . $_SESSION['username'] . '"';
        if ($conn->query($sql) === TRUE)
        {
            die("Success");
        }
    }
    else
    {
        die($_SESSION['username']);
    }

	$conn->close();
	header('Location: home.php');
	die;
}
?>

<!DOCTYPE html>
<html>
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Home Page</title>
    </head>
    <body class="bg-success">   
        <div class="container-full-bg ">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Sprint Retrospective</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="home.php">Home</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Account Settings<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="project.php">Projects</a>
                        </li>
                        <li class="nav-item">
                            <?php echo '<span class="navbar-text">' . 'Logged in as: '. $_SESSION['username'] . '</span>'; ?>
                        </li>
                    </ul> 
                </div>
            </nav>
            <div class="row" style="width:100%;">
                <div class="mb-1 col-10 col-lg-10 col-xl-10 col-sm-10 col-md-10 span6" style="float: none; margin: 0 auto;">
                    <form  method="POST" action="" class="username-form">
                        <label for="lblUsername">New Username:</label>
                        <input type="text" type="text" id="username" name="username">
                        <input type="submit" type="button" id="user" name="btnUser" value="Change username">
                    </form>
                </div>
            </div>
            <div class="row" style="width:100%;">
                <div class="mb-1 col-10 col-lg-10 col-xl-10 col-sm-10 col-md-10 span6" style="float: none; margin: 0 auto;">
                    <form  method="POST" action="" class="password-form">
                        <label for="lblPassword">New Password:&nbsp</label>
                        <input type="text" type="text" id="password" name="password">
                        <input type="submit" type="button" id="pass" name="btnPassword" value="Change password">
                    </form>
                </div>
            </div>
            <div class="row" style="width:100%;">
                <div class="mb-1 col-10 col-lg-10 col-xl-10 col-sm-10 col-md-10 span6" style="float: none; margin: 0 auto;">
                    <form  method="POST" action="" class="email-form">
                        <label for="lblEmail">New Email:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</label>
                        <input type="text" type="text" id="email" name="email">
                        <input type="submit" type="button" id="btnEmail" name="btnEmail" value="Update Email">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>