<?php
session_start();
if(!isset($_SESSION['username']))
{
    header('Location: index.php');
}
if(isset($_POST['add']))
{
    $name = $_POST['userToAdd'];
    $pid = $_GET['id'];
    $conn = new mysqli('localhost:3306','root','bob','users');
    $user_sql = 'SELECT id FROM user_info where username = "' . $name . '"';
    $user_result = $conn->query($user_sql) or die($conn->error);
    if ($user_result->num_rows > 0)
    {
        $row = $user_result->fetch_assoc();
        $sql = 'INSERT into project_users_junction (userId, projectId) VALUES (' . $row['id']. ',' . $pid .')';
        $conn->query($sql) or die($conn->error);
    }
}
if(isset($_POST['sprint']))
{
    $name = $_POST['sName'];
    $id = rand();
    $pid = $_GET['id'];
    $conn = new mysqli('localhost:3306','root','bob','users');
    $sql = 'INSERT into sprints (id, name,projectId) VALUES (' . $id .',"' . $name . '",' . $pid .')';
    $conn->query($sql) or die($conn->error);
}
if(isset($_POST['addToSprint']))
{
    $name = $_POST['sAddName'];
    $fid =  $_POST['feature_id'];
    $conn = new mysqli('localhost:3306','root','bob','users');
    $sql = 'SELECT * FROM sprints where name = "' . $name . '"';
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $ins_sql = 'INSERT into sprint_backlog_junction (sprintId, featureId) VALUES (' . $row['id'] .',' . $fid .')';
    $conn->query($ins_sql) or die($conn->error);
}

if(isset($_POST['create']))
{
    $conn = new mysqli('localhost:3306','root','bob','users');
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $stmt = $conn->prepare("INSERT INTO backlog (id,description, creator,type, points, projectid) VALUES (?,?,?,?,?,?)");
    $stmt->bind_param('isssii',$id,$desc,$creator,$type,$points,$projid);

    $id = rand();
    $desc = $_POST['fDesc'];
    $creator = $_SESSION['username'];
    $points = $_POST['fPoints'];
    $type = 'Feature';
    $projid = $_GET['id'];
    if(! $stmt->execute())
    {
        die("Internal Server Error!");
    }
    $stmt->close();
	$conn->close();
}
if(isset($_POST['log_time']))
{
    $conn = new mysqli('localhost:3306','root','bob','users');
    $sql = 'INSERT into user_timelog (userId,featureId,time) VALUES (' . $_POST['feature_id'] . ',' . $_SESSION['user_id'] . ',' . $_POST['time_to_log'] .')';
    $conn->query($sql) or die($conn->error);
}
?>


<!DOCTYPE html>

<html>
<div class="modal" id="createModal" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Feature</h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="">
                    <input type="text"  id = "fName" name="fName" placeholder="Feature Name" />
                    <input type="text"  id = "fPoints" name="fPoints" placeholder="Points" />
                    <input type="text"  id = "fDesc" name="fDesc" placeholder="Description:" />
                    <input type="submit" id="create" name="create" value="Create">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="sprintModal" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Sprint</h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="">
                    <input type="text"  id = "sName" name="sName" placeholder="Sprint Name" />
                    <input type="submit" id="sprint" name="sprint" value="Create">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="addToSprint" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Feature To Sprint</h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="POST" action="">
                    <input type="text"   id ="sAddName"    name="sAddName"    placeholder="Sprint Name" />
                    <input type="submit" id ="addToSprint" name="addToSprint" value="Add to Specified Sprint" />
                    <input type="text"   id ="feature_id"  name="feature_id"  readonly />
                    <br/>
                    <input type="text"   id ="time_to_log" name="time_to_log" placeholder="time" />  
                    <input type="submit" id ="log_time" name="log_time" value="Log" />    
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="showFeatures" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Features assosiated with this sprint: </h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="text"   id ="sprint_id"  name="sprint_id"  readonly />
                <ul>
                </ul>
            </div>
        </div>
    </div>
</div>
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Home Page</title>
        <script>
        add = (obj) =>{
            $(".modal-body #feature_id").val( obj.id );
            $("#addToSprint").modal();
            
        }
        get = (obj) =>{
            
            $.get( "api/backlogs.php?sprint_id="+obj.id, function( data ) {
                $("#showFeatures  .modal-body ul").remove();
                $("#showFeatures .modal-body").append('<ul style="list-style-type: none;margin: 0;padding: 0;"></ul>');    
                data = JSON.parse(data);
                for(let i = 0; i < data.length; ++i)
                {
                    $("#showFeatures .modal-body ul").append('<li style="font: 200 20px/1.5 Helvetica, Verdana, sans-serif;border-bottom: 1px solid #ccc;">Feature: '+ data[i].description +'<br/>Points: ' + data[i].points + '<br/>Created by: '+ data[i].creator +'</li>');    
                }
            });
            $(".modal-body #sprint_id").val( obj.id );
            $("#showFeatures").modal();
        }
        </script>
    </head>
    <body class="bg-success">   
        <div class="container-full-bg ">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Sprint Retrospective</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href="home.php">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="account.php">Account Settings</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="">Projects</a>
                        </li>
                        <li class="nav-item">
                            <?php echo '<span class="navbar-text">' . 'Logged in as: '. $_SESSION['username'] . '</span>'; ?>
                        </li>
                    </ul> 
                </div>
            </nav>

            <div id="fTable" style="position:relative;left:30%;overflow:scroll; height:40%; text-align: center; background:gray; width: 70%; outline: 1px solid black;">
                <h3 class="modal-title">Backlog</h3>
                <button class="btn btn-primary" style="width:100%;" type="button" id = "addFeature" data-toggle="modal" data-target="#createModal" >Add Feature</button>
                <?php
                    $conn = new mysqli('localhost:3306','root','bob','users');
                    $sql = 'SELECT * FROM backlog where projectid = ' . mysqli_real_escape_string($conn, $_GET['id']);
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0)
                    {
                        while($row = $result->fetch_assoc())
                        {
                                echo '<a onclick="add(this)" id=' . $row['id'] . '  class="list-group-item list-group-item-action" style="width:100%;text-align: center;"' . '<p>'. $row['description'] . '</p></a>';
                        }
                    }
                ?>
            </div>
            <div id="pTable" style="position:relative; left:30%;overflow:scroll; height:40%; text-align: center; background:gray; width: 70%; outline: 1px solid black;">
                <h3 class="modal-title">Sprints</h3>
                <button class="btn btn-primary" style="width:100%;" type="button" id = "addFeature" data-toggle="modal" data-target="#sprintModal" >Add Sprint</button>
                <?php
                    $conn = new mysqli('localhost:3306','root','bob','users');
                    $sql = 'SELECT * FROM sprints where projectId = ' . mysqli_real_escape_string($conn, $_GET['id']);
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0)
                    {
                        while($row = $result->fetch_assoc())
                        {
                                echo '<a onclick="get(this)" id=' . $row['id'] . '  class="list-group-item list-group-item-action" style="width:100%;text-align: center;"' . '<p>'. $row['name'] . '</p></a>';
                        }
                    }
                ?>
            </div>
            <div id='addUser' style="position:absolute; left: 1%;top:7.5%; overflow:hidden; height:25%; text-align: center; background:white; width: 10%; outline: 1px solid black;">
             
             <form method="post" action="">
                <input id="userToAdd" name="userToAdd" type="text">
                <input id="add" name="add" type="submit" value="Add User" style="width:100%">
             </form>
             
            </div>
            <div id = "userList" style="position:absolute; left: 1%;top:15.5%; overflow:hidden; height:25%; text-align: center; background:white; width: 10%; outline: 1px solid black;">
                <h3>Users:</h3>
                <ul style="list-style-type: none; margin: 0; padding: 0;">
                    <?php
                        $conn = new mysqli('localhost:3306','root','bob','users');
                        $user_sql = 'SELECT * FROM user_info';
                        $user_result = $conn->query($user_sql);
                        $data = array();
                        while ($row = mysqli_fetch_array($user_result))
                        {
                            $data[] = $row;
                        }

                        $project_sql = 'SELECT * FROM project_users_junction where projectId = ' . $_GET['id'];
                        $project_result = $conn->query($project_sql);
                        if ($project_result->num_rows > 0)
                        {
                            while($row = $project_result->fetch_assoc())
                            {
                                foreach ($data as $drow)
                                {
                                    if($drow['id'] == $row['userId'])
                                    {
                                        echo '<li>' . $drow['username'] . '</li>';
                                    }
                                }
                            }
                        }
                    ?>
                </ul>
            </div>
        </div>
    </body>
</html>