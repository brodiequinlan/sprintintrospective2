<?php
session_start();
if(!isset($_SESSION['username']))
{
    header('Location: index.php');
}

if(isset($_POST['create']))
{
    $name = $_POST['value'];
    $conn = new mysqli('localhost:3306','root','bob','users');
    if ($conn->connect_error) {
        die("Internal server Error!");
    }
    $stmt = $conn->prepare("INSERT INTO projects (name, id, owner_id) VALUES (?, ?, ?)");
    $stmt->bind_param('sii',$uname, $id,$o_id);

    $uname = $name;
    $id = rand();

    $sql = 'SELECT id FROM user_info where username = "'. $_SESSION['username']. '"'; 
    $result = $conn->query($sql);
    if ($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $o_id=$row['id'];
        }
    }
    try
    {
        $stmt->execute();
       
    }
	catch( mysqli_sql_exception $e )
    {
        echo 'Internal Server Error';
    }
	$stmt->close();
    $conn->close();
    header('Location: home.php');
    die;
}

?>



<div class="modal" id="createModal" data-backdrop="false" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create New Project</h4>
                <button class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="">
                <input type="text"  id = "value" name="value" placeholder="Project Name" />
                <input type="submit" id="create" name="create" value="Create">
                </form>
            </div>
        </div>
    </div>
</div>
        

<!DOCTYPE html>
<html>
    <head>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Home Page</title>
        <script>
        query = (obj) =>{
            window.location.replace("/project.php?id="+obj.id);
        }
        </script>
    </head>
    <body class="bg-success">   
        <div class="container-full-bg ">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Sprint Retrospective</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Home<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="account.php">Account Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="project.php">Projects</a>
                        </li>
                        <li class="nav-item">
                            <?php echo '<span class="navbar-text">' . 'Logged in as: '. $_SESSION['username'] . '</span>'; ?>
                        </li>
                    </ul> 
                </div>
            </nav>
            <div class="row" style="width:100%;">
                <div class="mb-1 col-10 col-lg-10 col-xl-10 col-sm-10 col-md-10 span6" style="float: none; margin: 0 auto;">

                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#createModal">Create Project</button>
                </div>
            </div>
            <div class="row" style="width:100%;">
                <div class="col-10 col-lg-10 col-xl-10 col-sm-10 col-md-10 span6" style="float: none; margin: 0 auto;">
                    
                    <?php
                        $conn = new mysqli('localhost:3306','root','bob','users');
                        $sql = 'SELECT * FROM projects where owner_id = ' . $_SESSION['user_id'];
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0)
                        {
                            while($row = $result->fetch_assoc())
                            {
                                    echo '<a onclick="query(this)" id=' . $row['id'] . '  class="list-group-item list-group-item-action" style="width:100%;text-align: center;"' . '<p>'. $row['name'] . '</p></a>';
                            }
                        }
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>