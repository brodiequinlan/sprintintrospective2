<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
    if(isset($_POST['login']))
    {
        $conn = new mysqli('localhost:3306','root','bob','users');
        $username = mysqli_real_escape_string($conn,$_POST['username']);
        $password = mysqli_real_escape_string($conn,$_POST['password']);
        $sql = 'SELECT * FROM user_info where username = "' . $username . '"';
        $result = $conn->query($sql);
        if ($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc()) {
                $g_pass = $row['password'];
                $salt = $row['salt'];
                if($g_pass == hash('sha256', $password . $salt))
                {
                    
                    session_start();
                    session_regenerate_id(true);
                    $_SESSION['username'] = $username;
                    $_SESSION['user_id'] = $row['id'];
                    header('Location: home.php');
                    die;
                }
                else
                {
                    echo '<script>alert("Incorrect password!");</script>';
                }
            }
        }
        else
        {
            echo '<script>alert("User does not exist!");</script>';
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <title>Home</title>
        <meta name="Home page" content="Home page">
        <meta name="brodie quinlan">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="static/css/login.css">
        <title>Login</title>
    </head>
    <body class="bg-success">   
        <div class="container-full-bg ">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="#">Sprint Retrospective</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active">
                            <a class="nav-link" href="#">Login<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="register.php">Register</a>
                        </li>
                    </ul>
                    
                </div>
            </nav>
            <div class="login-page">
                <div class="form">
                    <form  method="post" action="" class="login-form">
                        <input type="text" id="username" name="username" placeholder="username"/>
                        <input type="password" id="password" name="password" placeholder="password"/>
                        <input type="submit" type="button" id="login" name="login" value="Login">
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>